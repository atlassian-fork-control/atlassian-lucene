#!/bin/bash

export ANT_OPTS=-Xmx1g

version=$1
#ant clean remove-maven-artifacts
#ant -Dversion=$version generate-maven-artifacts

projects=`ls -la dist/maven/org/apache/lucene | grep 'lucene-' | cut -d ' ' -f14`
for project in $projects
do
	echo "Installing project $project"
	artifactFile=`ls dist/maven/org/apache/lucene/$project/$version/$project-*[0-9].jar`
	pomFile=`ls dist/maven/org/apache/lucene/$project/$version/$project-*.pom`
	sourcesFile=`ls dist/maven/org/apache/lucene/$project/$version/$project-*-sources.jar`
	mvn install:install-file -Dfile=$artifactFile -DgroupId=org.apache.lucene -DartifactId=$project -Dversion=$version -Dpackaging=jar -DpomFile=$pomFile -Dsources=$sourcesFile
	echo ""
done

luceneParentPom=`ls dist/maven/org/apache/lucene/lucene-parent/$version/lucene-parent-*-1.pom`
mvn install:install-file -DgroupId=org.apache.lucene -DartifactId=lucene-parent -Dversion=$version -Dpackaging=pom -Dfile=$luceneParentPom -DpomFile=$luceneParentPom

solrParentPom=`ls dist/maven/org/apache/lucene/lucene-solr-grandparent/$version/lucene-solr-grandparent-*-1.pom`
mvn install:install-file -DgroupId=org.apache.lucene -DartifactId=lucene-solr-grandparent -Dversion=$version -Dpackaging=pom -Dfile=$solrParentPom -DpomFile=$solrParentPom
